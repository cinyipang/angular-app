
export const environment = {
  production: false,
  auth: {
    clientId: 'B2BDF8E049EACD2F943406FB276791DF053B42205F41CDD72791EC7DF0730429',
    issuer: 'http://localhost:8080'
  },
  grapql: {
    endpoint: 'http://localhost:3001/graphql',
  },
  aes: {
    key: "6268714dc5ded9dd",
    iv: "5e93033b0f4d799a"
  },
  des: {
    key: "2d348676",
    iv: "49f7937a"
  }
};
  