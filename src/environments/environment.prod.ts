
export const environment = {
  production: true,
  auth: {
    clientId: 'FE9F699B7F12A031306E7648970226529404EFC994126E545D0992BD72139090',
    issuer: 'http://localhost:55855'
  },
  grapql: {
    endpoint: 'http://localhost:3001/graphql',
  },
  aes: {
    key: "6268714dc5ded9dd",
    iv: "5e93033b0f4d799a"
  },
  des: {
    key: "2d348676",
    iv: "49f7937a"
  }
};
  