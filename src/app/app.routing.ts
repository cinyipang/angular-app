import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuardService } from './services/guard.service'
import { ResumeComponent } from './layout/views/resume/resume.component';

export const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./layout/views/main/main.module').then(m => m.MainModule),
        canActivate: [GuardService]
    },
    {
        path: 'login',
        loadChildren: () => import('./layout/views/authentication/authentication.module').then(m => m.AuthenticationModule)
    },
    {
        path: 'resume',
        component: ResumeComponent,
        data: {
            title: 'Resume'
        }
    }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRouting {}
