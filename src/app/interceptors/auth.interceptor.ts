import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {}
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {    
    request = request
    .clone({
        setHeaders: {
            Authorization: `Bearer ${localStorage.getItem('access_token')}`
        }
    });

    return next.handle(request)
    .pipe(catchError((error: any) => {
        if (error.status === 401)
            window.location.href = '/login';
        throw error;
    }));
  }
}