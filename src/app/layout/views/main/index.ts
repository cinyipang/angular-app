import { HeaderComponent } from './shared/header/header.component';
import { SideMenuComponent } from './shared/side-menu/side-menu.component';
import { PageTitleComponent } from './shared/page-title/page-title.component';

import { HomeComponent } from './home/home.component';
import { DateComponent } from './date/date.component';
import { ProductComponent } from './product/product.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { HubComponent } from './hub/hub.component';
import { HubGroupComponent } from './hub-group/hub-group.component';
import { ProfileComponent } from './profile/profile.component';
import { FileComponent } from './file/file.component';
import { DemoComponent } from './demo/demo.component';

export {
    HeaderComponent,
    SideMenuComponent,
    PageTitleComponent,
    HomeComponent,
    HubComponent,
    HubGroupComponent,
    DateComponent,
    ProductComponent,
    ProductFormComponent,
    ProfileComponent,
    FileComponent,
    DemoComponent
}; 
