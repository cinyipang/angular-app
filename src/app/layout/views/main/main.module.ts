import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageCropperModule } from 'ngx-image-cropper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgMaterialMultilevelMenuModule } from 'ng-material-multilevel-menu';
import { MatIconModule } from '@angular/material/icon';
import { MainRouting } from './main.routing';
import { NgsRevealModule } from 'ngx-scrollreveal';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { ShredComponentModule } from '../../components/shared-component.module';

import { MainComponent } from './main.component';
import { PageTitleComponent, HomeComponent, HubComponent, HubGroupComponent, HeaderComponent, SideMenuComponent,
  DateComponent, ProductComponent, ProductFormComponent, ProfileComponent, FileComponent, DemoComponent} from 'src/app/layout/views/main';

@NgModule({
  declarations: [
    MainComponent,
    HeaderComponent,
    SideMenuComponent,
    HomeComponent,
    HubComponent,
    HubGroupComponent,
    DateComponent,
    ProductComponent,
    ProductFormComponent,
    ProfileComponent,
    FileComponent,
    DemoComponent,
    PageTitleComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    MainRouting,
    NgSelectModule,
    MomentModule,
    ImageCropperModule,
    NgMaterialMultilevelMenuModule,
    MatIconModule,
    NgsRevealModule,
    UiSwitchModule,
    ShredComponentModule,
  ],
  providers: [      
  ],
  bootstrap: [MainComponent],
})
export class MainModule { 
}
