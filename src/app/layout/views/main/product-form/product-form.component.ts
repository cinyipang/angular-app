import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { BrandApiService, CurrencyApiService, MaterialApiService, ProductApiService } from 'src/app/services/api/index';
import { CommonService } from 'src/app/services/index';
import { Product, Brands, Currencies, Materials } from 'src/app/models';

@Component({
    selector: 'app-product-form',
    templateUrl: './product-form.component.html',
    styleUrls: ['./product-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ProductFormComponent implements OnInit {

    constructor(private brandApi: BrandApiService, private currencyApi: CurrencyApiService, 
        private materialApi: MaterialApiService, private productApi: ProductApiService, 
        private toastr: ToastrService, private formBuilder: FormBuilder, private router: Router, 
        private route: ActivatedRoute, private common: CommonService){
    }

    submitted = false;
    isLoading = true;
    productId: string = this.route.snapshot.params['id'];
    materials: Materials =new Materials();
    currencies: Currencies = new Currencies();
    brands: Brands = new Brands();
    product: Product = new Product();
    productForm: FormGroup = this.formBuilder.group({
        code: ['', [Validators.required]],
        name: ['', [Validators.required]],
        brandId: ['', [Validators.required]],
        currencyId: ['', [Validators.required]],
        cost: ['', [Validators.required]],
        price: ['', [Validators.required]],
        minOfQuantity: ['', [Validators.required]],
        standardCarton: ['', [Validators.required]],
        isFeatured: [false],
        productMaterials: this.formBuilder.array([])
    });

    ngUnsubscribe: Subject<any> = new Subject();
    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
    ngOnInit(){
        if(this.productId)
        {
            this.productApi.get(this.productId)
            .subscribe(
                res => {
                    console.log(res);  
                    this.product = res; 
                    this.productForm = this.formBuilder.group({
                        code: [this.product.code, [Validators.required]],
                        name: [this.product.name, [Validators.required]],
                        brandId: [this.product.brand.brandId, [Validators.required]],
                        currencyId: [this.product.currency.currencyId, [Validators.required]],
                        cost: [this.product.cost, [Validators.required]],
                        price: [this.product.price, [Validators.required]],
                        minOfQuantity: [this.product.minOfQuantity, [Validators.required]],
                        standardCarton: [this.product.standardCarton, [Validators.required]],
                        isFeatured: [this.product.isFeatured],
                        productMaterials: this.formBuilder.array([])
                    });
                    let productMaterials = this.productForm.get('productMaterials') as FormArray;
                    if(this.product.productMaterials)
                    {
                        this.product.productMaterials.forEach(item => {
                            productMaterials.push(this.formBuilder.group({
                                materialId: [item.materialId, [Validators.required]],
                                quantity: [item.quantity, [Validators.required]]
                            }));
                        });
                    }
                    this.isLoading = false;
                },
                error => {
                    console.log("error", error);
                    this.toastr.warning("Product not found");
                    this.router.navigateByUrl('/product');   
                }
            );
        }
        else
            this.isLoading = false;
        
        this.addProductMaterial();
        this.getBrands();
        this.getCurrencies();
        this.getMaterials();
    }

    getBrands(){
        this.brandApi.find({ getAll: true })
        .subscribe(
            res => {
                console.log(res);   
                this.brands = res;  
            },
            error => {
                console.log("error", error);
            }
        );
    }

    getCurrencies(){
        this.currencyApi.find({ getAll: true })
        .subscribe(
            res => { 
                console.log(res);   
                this.currencies = res; 
            },
            error => {
                console.log("error", error);
            }
        );
    }
    
    getMaterials(){
        this.materialApi.find({ getAll: true })
        .subscribe(
            res => {
                console.log(res);   
                this.materials = res;  
            },
            error => {
                console.log("error", error);
            }
        );
    }

    addProductMaterial(){
        let productMaterials = this.productForm.get('productMaterials') as FormArray;
        productMaterials.push(this.formBuilder.group({
            materialId: ['', [Validators.required]],
            quantity: ['', [Validators.required]]
        }));
    }

    removeProductMaterial(i){
        let productMaterials = this.productForm.get('productMaterials') as FormArray;
        productMaterials.removeAt(i);
    }

    get formProductMaterials() { return this.productForm.get('productMaterials') as FormArray; }

    submit(){
        this.submitted = true;

        console.log(this.productForm);

        if(this.productForm.invalid)
            return;
        
        this.isLoading = true;
        if(this.productId)
        {
            this.productApi.update(this.productId, this.productForm.value)
            .subscribe(
                res => {
                    console.log(res);
                    this.router.navigateByUrl('/product');    
                    this.toastr.success("Product has been updated", "Success!");
                },
                error => {
                    console.log("error", error);
                    this.toastr.error("Unable to update product", "Error!");
                    this.isLoading = false;
                }
            );
        }
        else
        {
            this.productApi.add(this.productForm.value)
            .subscribe(
                res => {
                    console.log(res);
                    this.router.navigateByUrl('/product');    
                    this.toastr.success("Product has been created", "Success!");
                },
                error => {
                    console.log("error", error);
                    this.toastr.error("Unable to create product", "Error!");
                    this.isLoading = false;
                }
            );
        }
    }
}