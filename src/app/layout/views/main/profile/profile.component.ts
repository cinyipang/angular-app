import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'src/app/services/common.service';
import { UserApiService } from 'src/app/services/api/index';
import { User } from 'src/app/models/index';
import { takeUntil } from 'rxjs-compat/operator/takeUntil';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {

    constructor(private common: CommonService, private userApi: UserApiService, 
        private toastr: ToastrService, private formBuilder: FormBuilder){
    }

    ngUnsubscribe: Subject<any> = new Subject();

    profilePicture: string = null;
    user = new User();
    isUploading = false;
    isLoading = false;
    submitted = false;
    imageChangedEvent: any = '';
    croppedImage: any = '';
    profileForm: FormGroup = this.formBuilder.group({
        name: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        remarks: ['', []]
    });

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
    ngOnInit() {
        this.userApi.getCurrentUserObservable()
        .subscribe(
            res => {
                //waiting for getCurrentUser done 
                if(res)
                {
                    this.user = res;
                    this.profileForm = this.formBuilder.group({
                        name: [this.user.name, [Validators.required]],
                        email: [this.user.email, [Validators.required, Validators.email]],
                        remarks: [this.user.remarks, []]
                    });
                }
            },
            error => {
            }
        );
    }

    updateProfile(){
        console.log(this.profileForm)
        this.submitted = true;

        if(this.profileForm.invalid)
            return;
        
        this.isLoading = true;
        console.log(this.profileForm.value);
        this.userApi.profile(this.profileForm.value)
        .subscribe(
            res => {
                console.log(res);   
                this.user = res;
                this.userApi.updateCurrentUserObservable(this.user);
                this.isLoading = false;
                this.toastr.success("Your profile has been successfully updated!");
            },
            error => {
                console.log("error", error);
                this.isLoading = false;
            }
        );
    }
    
    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
    }
    imageCropped(event: ImageCroppedEvent) {
        this.croppedImage = event.base64;
    }
    imageLoaded() {
        console.log("show cropper");
        document.getElementById("btn_open_modal").click();
    }
    loadImageFailed() {
        this.toastr.error("Unable to load image", "Error");
    }
    onUploadProfilePic(){
        const imageBlob = this.common.dataUriToBlob(this.croppedImage);
        const imageFile = new File([imageBlob], "profile.png", { type: 'image/png' });

        console.log(imageFile.size);
        if(imageFile.size/1024/1024 > 1)
        {
            this.toastr.warning("file is not allowed to bigger than 1MB");
            return;
        }

        //Upload Profile
        this.isUploading = true;
        var formData = new FormData();
        formData.append('file', imageFile);

        this.userApi.profilePicture(formData)
        .subscribe(
            res => {
                console.log(res)
                this.user.profilePicture = res.profilePicture;
                this.userApi.updateCurrentUserObservable(this.user);
                this.isUploading = false;
                this.croppedImage = '';
                document.getElementById("btn_close_modal").click();
            },
            error => {
                this.toastr.error("Upload profile picture failed!", "Error");
                this.isUploading = false;
            }
        );
    }
}
