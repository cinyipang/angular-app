import { Component, ViewEncapsulation } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NgbDateStruct, NgbTimeStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';
import * as moment from 'moment';

@Component({
    selector: 'app-date',
    templateUrl: './date.component.html',
    styleUrls: ['./date.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DateComponent {
    minDate: NgbDateStruct;    
    maxDate: NgbDateStruct;   

    dob: NgbDateStruct;

    appDate: NgbDateStruct;    
    appTime: NgbTimeStruct;
    appDateTime: Date;
    dateNow: string;
    date: any;
    meridian = true;
    formattedDateTime;

    constructor(private calendar: NgbCalendar, private datePipe: DatePipe, private comm: CommonService) {}

    ngOnInit(){
        this.minDate = { year: 1930, month: 1, day: 1 };
        this.maxDate = this.calendar.getToday();

        this.appTime = { hour: 10, minute: 0, second: 0 };
        this.appDate = this.calendar.getToday();

        this.dob = this.comm.parseDateStruct("1990-06-26");
    }
    
    generateDateTime()
    {
        this.appDateTime = this.comm.parseDateTime(this.appDate, this.appTime);
        this.formattedDateTime = this.datePipe.transform(this.appDateTime, 'dd MMMM yyyy, hh:mm a');
        
        //use "moment" or "DatePipe" instead of "new Date" to pass the value to API
        console.log(moment(new Date).format());
        console.log(this.datePipe.transform(new Date, 'yyyy-MM-dd HH:mm:ss'));
    }
}