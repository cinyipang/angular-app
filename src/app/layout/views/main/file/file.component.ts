import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';
import { MediaApiService } from 'src/app/services/api/index';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FileComponent implements OnInit {

    constructor(private mediaApi: MediaApiService, private toastr: ToastrService){
    }

    ngUnsubscribe: Subject<any> = new Subject();
    fileEvent: any;
    isDowdloading = false;
    isUploading = false;
    csvFiles : FileList; 
    excelFiles : FileList; 

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
    ngOnInit() {
    }

    onLoadCsv(event){
        this.csvFiles = event.target.files;

        if(event.target.files)
        {
            var fileName = event.target.files[0].name;
            var nextSibling = event.target.nextElementSibling
            nextSibling.innerText = fileName
        }
    }
    onUploadCsv(){
        if(!this.csvFiles)
            return;
        
        const fileType = this.csvFiles[0].name.split('.').pop();
        if(fileType !== 'csv')
        {
            this.toastr.warning('Invalid file type');
            return;
        }

         //Upload Csv
        this.isUploading = true;
        var formData = new FormData();
        formData.append('files', this.csvFiles[0]);
        formData.append('description', 'upload csv');

        this.mediaApi.uploadCsv(formData)
        .subscribe(
            res => {
                console.log(res);   
                this.isUploading = false;
            },
            error => {
                console.log("error", error);
                this.toastr.error(error.error);
                this.isUploading = false;
            }
        );
    }

    async downloadPdf()
    { 
        this.isDowdloading = true;
        const params = new HttpParams().set('type', 'pdf');       
        const isSuccess = await this.mediaApi.downloadPdf(params);
        this.isDowdloading = false;
        console.log("result", isSuccess);
    }

    onLoadExcel(event){
        this.excelFiles = event.target.files; 
    }
    onUploadExcel(){
        if(!this.excelFiles.length)
            return;

        const fileType = this.excelFiles[0].name.split('.').pop();
        if(fileType !== 'xlsx' && fileType !== 'xls')
        {
            this.toastr.warning('Invalid file type');
            return;
        }

        //Upload excel
        this.isUploading = true;
        var formData = new FormData();
        formData.append('files', this.excelFiles[0]);
        formData.append('description', 'upload excel');

        this.mediaApi.uploadExcel(formData)
        .subscribe(
            res => {
                console.log(res);   
                this.isUploading = false;
            },
            error => {
                console.log("error", error);
                this.toastr.error(error.error);
                this.isUploading = false;
            }
        );
    }

    async downloadExcel()
    { 
        this.isDowdloading = true;
        const params = new HttpParams().set('type', 'excel');
        const isSuccess = await this.mediaApi.downloadExcel(params);
        this.isDowdloading = false;
    }

    async downloadCsv()
    {
        this.isDowdloading = true;
        const isSuccess = await this.mediaApi.downloadCsv();
        this.isDowdloading = false;
    }
}
