import { Component, ViewEncapsulation } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-demo',
    templateUrl: './demo.component.html',
    styleUrls: ['./demo.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DemoComponent {
    constructor(private comm: CommonService, private formBuilder: FormBuilder, private toastr: ToastrService) {}

    submitted = false;
    isLoading = false;
    isDownloading = false;
    enableSwitch = false;
    file: File;
    fileBase64: any;
    brands = [{ name: 'Brand A', brandId: 1 }, { name: 'Brand B', brandId: 2 }];
    demoForm: FormGroup = this.formBuilder.group({
        brandId: ['', [Validators.required]],
        isActive: [true, [Validators.required]],
        productCodeA: [''],
        productCodeB: ['']
    });

    ngOnInit(){
    }

    submit(){
        if(this.demoForm.value.brandId === 1)
        {
            this.demoForm.controls.productCodeA.setValidators([Validators.required]);
            this.demoForm.controls.productCodeB.setValidators(null);
        }
        else if(this.demoForm.value.brandId === 2)
        {
            this.demoForm.controls.productCodeB.setValidators([Validators.required]);
            this.demoForm.controls.productCodeA.setValidators(null);
        }
        else
        {
            this.demoForm.controls.productCodeA.setValidators(null);
            this.demoForm.controls.productCodeB.setValidators(null);
        }
        this.demoForm.controls.productCodeA.updateValueAndValidity();
        this.demoForm.controls.productCodeB.updateValueAndValidity();

        this.submitted = true;
        console.log(this.demoForm);

        if(this.demoForm.invalid)
            return;
    }

    sweetAlert()
    {
        Swal({
            title: 'Are you sure?',
            text: `Delete item`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            confirmButtonColor: '#d33'
        })
        .then((result) => {
            if (result.value) {                
                Swal(
                    'Deleted!',
                    `Item has been deleted.`,
                    'success'
                );
            } 
        });
    }

    sweetAlertOk(){
        Swal({
            title: 'Important Message',
            text: `Hello World`,
            type: 'success',
            confirmButtonText: 'Close',
        })
    }

    successToastr(){
        this.toastr.success("Product has been updated", "Success!");
    }
    warningToastr(){
        this.toastr.warning("This is a warning message", "Warning!");
    }

    onChangeSwitch(value){
        console.log(value);
    }

    download(){
        this.isDownloading = true;
        setTimeout(function(){ this.isDownloading = false; }, 2000);
    }

    async onLoad(event){
        if(!event.target.files) return;
        
        this.file = event.target.files[0];
        var fileName = event.target.files[0].name;
        var nextSibling = event.target.nextElementSibling
        nextSibling.innerText = fileName;

        const photoFileExtension = fileName.split('.').pop();
        if(!['jpeg', 'jpg', 'png', 'gif'].includes(photoFileExtension.toLowerCase()))
        {
            this.file = null;
            alert('Invalid file type');
            return;
        }
        this.fileBase64 = await this.comm.readFileToBase64(this.file);
        console.log('imageUrl', this.fileBase64)
    }
    onUpload(){
        console.log(this.file)
    }
}