import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent, HubComponent, HubGroupComponent, DateComponent, 
    ProductComponent, ProductFormComponent, ProfileComponent, FileComponent, DemoComponent } from 'src/app/layout/views/main';
import { GuardService } from 'src/app/services/guard.service'
import { MainComponent } from './main.component';

export const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        canActivate: [GuardService],
        children:[
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                component: HomeComponent,
                canActivate: [GuardService],
                data: {
                    title: 'Home'
                }
            },
            {
                path: 'hub/connection',
                component: HubComponent,
                canActivate: [GuardService],
                data: {
                    title: 'Hub Connection'
                }
            },
            {
                path: 'hub/group',
                component: HubGroupComponent,
                canActivate: [GuardService],
                data: {
                    title: 'Hub Group'
                }
            },
            {
                path: 'profile',
                component: ProfileComponent,
                canActivate: [GuardService],
                data: {
                    title: 'Profile'
                }
            },
            {
                path: 'file',
                component: FileComponent,
                canActivate: [GuardService],
                data: {
                    title: 'File'
                }
            },
            {
                path: 'date',
                component: DateComponent,
                canActivate: [GuardService],
                data: {
                    title: 'Date'
                }
            },
            {
                path: 'product',
                component: ProductComponent,
                canActivate: [GuardService],
                data: {
                    title: 'Product',
                    permission: {
                        module: 'Product',
                        access: 'Read'
                    }
                }
            },
            {
                path: 'product/form',
                component: ProductFormComponent,
                canActivate: [GuardService],
                data: {
                    title: 'Add Product',
                    permission: {
                        module: 'Product',
                        access: 'Create'
                    }
                }
            },
            {
                path: 'product/form/:id',
                component: ProductFormComponent,
                canActivate: [GuardService],
                data: {
                    title: 'Edit Product',
                    permission: {
                        module: 'Product',
                        access: 'Update'
                    }
                }
            },
            {
                path: 'demo',
                component: DemoComponent,
                canActivate: [GuardService],
                data: {
                    title: 'Demo'
                }
            }
        ]
    },
    
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class MainRouting {}
