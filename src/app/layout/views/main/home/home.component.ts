import { Component, ViewEncapsulation } from '@angular/core';
import { Chart } from 'chart.js';
import * as AOS from 'aos';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent {
    constructor() {}

    lineChart = []; 
    barChart = []; 
    fadeInTop = { delay: 200, duration: 500, origin: 'top', distance : '100px'};
    images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/300`);

    ngOnInit() {
        let lineCtx = document.getElementById("line_chart");
        this.lineChart = new Chart(lineCtx, {
            type: 'line',
            data: {
              labels: ["January", "February", "March", "April", "May", "June"],
              datasets: [{ 
                    label: "Personal",
                    data: [6500, 5900, 4000, 7800, 5600, 5500],
                    borderColor: "#3cba9f",
                    backgroundColor: "rgba(31,166,122,0.3)"
                },
                {
                    label: "Business",
                    data: [4800, 4800, 5500, 4100, 8000, 5100],
                    borderColor: "#4997e4",
                    backgroundColor: "rgba(73,151,228,0.3)"
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            stepSize: 1000,
                            max: 9000,
                            min: 3000 //max value for the chart
                        }
                    }]
                }
            }
        });

        let barCtx = document.getElementById("bar_chart");
        this.barChart = new Chart(barCtx, {
            type: 'bar',
            data: {
              labels: ["January", "February", "March", "April", "May", "June"],
              datasets: [{ 
                    label: "Personal",
                    data: [6500, 5900, 4000, 7800, 5600, 5500],
                    borderColor: "#f30000",
                    backgroundColor: "rgba(255,0,0,0.3)"
                },
                {
                    label: "Business",
                    data: [4800, 4800, 5500, 4100, 8000, 5100],
                    borderColor: "#ffff25",
                    backgroundColor: "rgba(255,255,37,0.3)"
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            stepSize: 2000,
                            max: 10000 //max value for the chart
                        }
                    }]
                }
            }
        });

        //delay for element ready
        setTimeout(()=>{
            AOS.init();
        }, 500)
    }
}
