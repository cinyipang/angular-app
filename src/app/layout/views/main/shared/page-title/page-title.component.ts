import { Component, Input, ViewEncapsulation } from '@angular/core';
import { CommonService } from 'src/app/services/index';

@Component({
    selector: 'page-title',
    templateUrl: './page-title.component.html',
    styleUrls: ['./page-title.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class PageTitleComponent {
    constructor(private common: CommonService){
    }

    @Input() backButton: Boolean = true;
    @Input() label: string;
}