import { Component, ViewEncapsulation, Input, ElementRef, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/oauth.service';
import { UserApiService } from 'src/app/services/api/index';
import { User } from 'src/app/models';
import { TimelineLite } from 'gsap';
import { CommonService } from 'src/app/services';

@Component({
    selector: 'side-menu',
    templateUrl: './side-menu.component.html',
    styleUrls: ['./side-menu.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class SideMenuComponent {
    constructor(private userApi: UserApiService, private authService: AuthService , 
        public commonService: CommonService , private location: Location) {
    }
    
    timelineLite = new TimelineLite();
    user = new User();
    isToggleMenu: boolean;
    activeUrl = "";
    config = {
        paddingAtStart: true,
        interfaceWithRoute: true,
        classname: 'material-menu',
        fontColor: `#989898`,
        selectedListFontColor: `#fff`,
        highlightOnSelect: true,
        collapseOnSelect: true,
        rtlLayout: false
    };
    appitems = [
        {
            label: 'Item 1',
            icon: 'favorite_border',    //mat-icon
            items: [
                {
                    label: 'Item 1.1',
                    link: '/product',
                    faIcon: 'fab fa-accusoft'   //fa-icon
                },
                {
                    label: 'Item 1.2',
                    faIcon: 'fab fa-accessible-icon',
                    items: [
                        {
                            label: 'Item 1.2.1',
                            link: '/product',
                            faIcon: 'fas fa-ambulance',
                        },
                        {
                            label: 'Item 1.2.2',
                            faIcon: 'fas fa-anchor',
                        },
                        {
                            label: 'Item 1.2.3',
                            faIcon: 'fas fa-anchor',
                        },
                        {
                            label: 'Item 1.2.4',
                            faIcon: 'fas fa-anchor',
                        },
                        {
                            label: 'Item 1.2.4',
                            faIcon: 'fas fa-anchor',
                        },
                        {
                            label: 'Item 1.2.4',
                            faIcon: 'fas fa-anchor',
                        }
                    ]
                }
            ]
        },
        {
            label: 'Product',
            faIcon: 'fab fa-product-hunt',
            items: [
                {
                    label: 'View',
                    link: '/product',
                },
                {
                    label: 'New',
                    link: '/product/form',
                }
            ]
        },
        {
            label: 'Hub',
            faIcon: 'fab fa-hubspot',
            items: [
                {
                    label: 'By Connection',
                    link: '/hub/connection',
                },
                {
                    label: 'By Group',
                    link: '/hub/group',
                }
            ]
        },
        {
            label: 'Date',
            faIcon: 'far fa-calendar-alt',
            link: '/date',
        },
        {
            label: 'File',
            faIcon: 'far fa-file-pdf',
            link: '/file',
        },
        {
            label: 'Demo',
            faIcon: 'far fa-bookmark',
            link: '/demo',
        }
    ];

    ngAfterViewInit(){
        let headers = window.document.getElementsByClassName('nav-header');
        this.timelineLite.from(headers, 0.5, {right:80, opacity:0});

        let matListItems = window.document.getElementsByClassName('mat-list-item');
        for (let i=0; i < matListItems.length; i++)
            matListItems[i].removeAttribute('title'); // removes the 'title' attribute
    }

    ngOnInit(){
        this.activeUrl = this.location.path();
        this.getCurrentUser();
    }

    logout(){
        this.authService.logout();
    }
    
    getCurrentUser(){ 
        this.userApi.getCurrentUserObservable().subscribe(res => {
            this.user = res;
        });
    }

    toggleMenu(){
        this.commonService.isToggleMenu = !this.commonService.isToggleMenu;
    }
}
  
// $(document).ready(function() {
//   $(".material-menu .mat-list-item").removeAttr('title');
// });