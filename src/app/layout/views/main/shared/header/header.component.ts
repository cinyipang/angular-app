import { Component, ViewEncapsulation, Input } from '@angular/core';
import { AuthService } from 'src/app/services/oauth.service';
import { CommonService } from 'src/app/services';


@Component({
    selector: 'layout-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class HeaderComponent {
    constructor(private authService: AuthService, private commonService: CommonService) {
    }

    logout(){
        this.authService.logout();
    }

    toggleMenu(){
        this.commonService.isToggleMenu = !this.commonService.isToggleMenu;
    }
}