import { Component, ViewEncapsulation } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-hub',
    templateUrl: './hub.component.html',
    styleUrls: ['./hub.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HubComponent {
    connectionId: String;
    submitted = false;
    isLoadingSubmit = false;
    hubConnection: HubConnection;
    hubForm: FormGroup = this.formBuilder.group({
        connectionId: ['', [Validators.required]],
        sender: ['', [Validators.required]],
        message: ['', [Validators.required]],
    });
    ngUnsubscribe: Subject<any> = new Subject();
    headers = new HttpHeaders({
        'Content-Type':  'application/json'
    });

    constructor(private toastr: ToastrService, private formBuilder: FormBuilder,
         private http: HttpClient){
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
    ngOnInit(){
        this.hubConnection = new HubConnectionBuilder().withUrl(environment.auth.issuer + '/chatHub').build();

        this.hubConnection
        .start()
        .then(() => 
            this.hubConnection.invoke('getConnectionId')
            .then((connectionId) => {
                console.log("connectionId", connectionId);
                this.connectionId = connectionId;
            })
        )
        .catch(err => console.log(err));

        this.hubConnection.on("ReceiveMessageConnection", (sender, message) => {
            this.generateMessage(sender, message);
        });
    }
    
    generateMessage(user, message): void {
        let msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        let encodedMsg = user ? user + ": " + msg : msg;
        let li = document.createElement("li");
        li.textContent = encodedMsg;
        document.getElementById('messages_list').appendChild(li);
    }

    sendMessage()
    {
        this.submitted = true;

        if(this.hubForm.invalid)
            return;

        console.log(this.hubForm.value);
        this.http.post(environment.auth.issuer + "/api/hubs/chat/message", this.hubForm.value, { headers: this.headers })
        .pipe(map((res: any) => res))
        .subscribe(
            res => {
                this.hubForm.patchValue({ message: '' });
                this.isLoadingSubmit = false;
                this.submitted = false;
            },
            error => {
                console.log("error", error);
                this.toastr.error("Unable to send message", "Error!");
                this.isLoadingSubmit = false;
            }
        );
    }
}