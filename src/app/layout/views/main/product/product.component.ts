import { Component, ElementRef, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import 'rxjs/Rx';
import Swal from 'sweetalert2';
import { ProductApiService } from 'src/app/services/api/index';
import { Products, Param } from 'src/app/models';
import { PermissionService } from 'src/app/services';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ProductComponent implements OnInit {
    @ViewChild('searchRef', { static: true }) searchRef: ElementRef;

    constructor(private productApi: ProductApiService, private permissionService: PermissionService){
    }

    products: Products = new Products();
    params: Param[] = [];
    isLoading: Boolean = false;
    ngUnsubscribe: Subject<any> = new Subject();
    canUpdate: Boolean = false;
    canDelete: Boolean = false;

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
    async ngOnInit(){
        this.canUpdate = await this.permissionService.hasAccess('Product', 'Update');
        this.canDelete = await this.permissionService.hasAccess('Product', 'Delete');
        console.log('canUpdate', this.canUpdate, 'canDelete', this.canDelete)

        this.getProducts();

        Observable.fromEvent(this.searchRef.nativeElement, 'keyup')
        .map((event: any) => event.target.value)
        .debounceTime(1000)         
        .distinctUntilChanged()
        .subscribe((text: string) => this.enterSearch(text));
    }

    getProducts()
    {
        if(this.isLoading)
            return;
        this.isLoading = true;
        this.productApi.find(this.products.pagination)
        .subscribe(
            res => {
                console.log(res);
                this.products = res;
                this.isLoading = false;
            },
            error => {
                console.log("error", error);
                this.isLoading = false;
            }
        );
    }

    sortBy(order)
    {
        this.products.pagination.page = 1;
        this.products.pagination.order = order;
        this.products.pagination.asc = !this.products.pagination.asc;
        this.getProducts();
    }

    changePage(page){
        this.products.pagination.page = page;
        this.getProducts();
    }

    enterSearch(text){
        this.params.length = 0;
        this.params.push({ value: text, key: 'Code', type: 'Contains' });
        this.params.push({ value: text, key: 'Brand.Code', type: 'Contains' });
        this.products.pagination.params = this.params;
        this.products.pagination.operation = 'Or';
        this.products.pagination.page = 1;
        this.getProducts();
    }

    deleteAlert(name, id)
    {
        Swal({
            title: 'Are you sure?',
            text: `Delete product: ${name}`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            confirmButtonColor: '#d33'
        })
        .then((result) => {
            if (result.value) {
                this.productApi.delete(id)
                .subscribe(
                    res => { 
                        this.getProducts();
                        Swal('Deleted!',`Product ${name} has been deleted.`,'success');
                    },
                    error => {
                        console.log("error", error);
                    }
                );
            } 
        });
    }
}