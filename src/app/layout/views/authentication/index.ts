import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register/register.component';

export {
    LoginComponent,
    ForgotPasswordComponent,
    RegisterComponent
}; 
