import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { AuthService } from 'src/app/services/oauth.service';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
    ngUnsubscribe: Subject<any> = new Subject();
    submitted = false;
    isLoading = false;
    loginForm: FormGroup;

    constructor(private oauth: AuthService, private formBuilder: FormBuilder, 
        private toastr: ToastrService){
        this.oauth.clearAuthentication();
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required]],
            password: ['', [Validators.required, Validators.minLength(8)]]
        });
      }

    get f() { return this.loginForm.controls; }

    onLogin(){
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid)
            return;

        this.isLoading = true;
                        
        this.oauth.login(this.loginForm.value.username, this.loginForm.value.password)
        .pipe(map((res: any) => res))
        .subscribe(
            res => {
                var jwtHelper = new JwtHelperService();
                var decodedToken = jwtHelper.decodeToken(res.token);
                localStorage.setItem(this.oauth.storageKeys.accessToken, res.token);
                localStorage.setItem(this.oauth.storageKeys.expiresAt, decodedToken.exp);
                window.location.href  = '/';
            },
            error => {
                console.log("error", error);
                this.toastr.error('Invalid username or password', 'Error!');
                this.isLoading = false;
            }
        );
    }
}