import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
    
    ngUnsubscribe: Subject<any> = new Subject();

    constructor(){
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
    ngOnInit(): void {
    }
}