import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AuthenticationRouting } from './authentication.routing';
import { ShredComponentModule } from '../../components/shared-component.module';

import { LoginComponent, ForgotPasswordComponent, RegisterComponent } from 'src/app/layout/views/authentication';
import { AuthenticationComponent } from './authentication.component';

@NgModule({
  declarations: [
    AuthenticationComponent,
    LoginComponent,
    ForgotPasswordComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    AuthenticationRouting,
    ShredComponentModule,
  ],
  providers: [      
  ],
  bootstrap: [AuthenticationComponent],
})
export class AuthenticationModule { 
}
