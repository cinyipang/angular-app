import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ForgotPasswordComponent implements OnInit {
    
    ngUnsubscribe: Subject<any> = new Subject();
    submitted = false;
    isLoading = false;
    form: FormGroup;

    constructor(private formBuilder: FormBuilder){
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
    ngOnInit(): void {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }

    get f() { return this.form.controls; }

    onSubmit(){
        this.submitted = true;

        // stop here if form is invalid
        if (this.form.invalid)
            return;
    }
}