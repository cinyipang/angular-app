import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResumeComponent { 
    constructor(){
    }
    
    backendSkills: Array<Skill> = new Array<Skill>();
    frontendSkills: Array<Skill> = new Array<Skill>();
    databaseSkills: Array<Skill> = new Array<Skill>();
    security: Array<Skill> = new Array<Skill>();
    softwares: Array<Skill> = new Array<Skill>();
    jobDescriptions: Array<JobDescription> = new Array<JobDescription>();
    interships: Array<JobDescription> = new Array<JobDescription>();
    educations: Array<Education> = new Array<Education>();

    ngOnInit(){      
      let skill = new Skill();
      skill.label = "ASP.NET 4.X";
      skill.full = 5;
      this.backendSkills.push(skill);
      
      skill = new Skill();
      skill.label = ".NET CORE 2.x";
      skill.full = 5;
      this.backendSkills.push(skill);

      skill = new Skill();
      skill.label = "Node JS API";
      skill.full = 4;
      this.backendSkills.push(skill);
      
      skill = new Skill();
      skill.label = "OAuth 2.x";
      skill.full = 5;
      this.backendSkills.push(skill);
      
      skill = new Skill();
      skill.label = "EntityFramework/EF Core";
      skill.full = 5;
      this.backendSkills.push(skill);
      
      skill = new Skill();
      skill.label = "Azure Cloud Serivces";
      skill.full = 4;
      this.backendSkills.push(skill);

      skill = new Skill();
      skill.label = "Amazon Web Services";
      skill.full = 4;
      this.backendSkills.push(skill);

      //database
      skill = new Skill();
      skill.label = "MongoDB";
      skill.full = 5;
      this.databaseSkills.push(skill);
      
      skill = new Skill();
      skill.label = "MSSQL";
      skill.full = 5;
      this.databaseSkills.push(skill);
      
      skill = new Skill();
      skill.label = "Firebase";
      skill.full = 4;
      this.databaseSkills.push(skill);
      
      skill = new Skill();
      skill.label = "MYSQL";
      skill.full = 4;
      this.databaseSkills.push(skill);

      //frontend
      skill = new Skill();
      skill.label = "Angular";
      skill.full = 5;
      this.frontendSkills.push(skill);

      skill = new Skill();
      skill.label = "Vue JS";
      skill.full = 5;
      this.frontendSkills.push(skill);

      skill = new Skill();
      skill.label = "React JS";
      skill.full = 4;
      this.frontendSkills.push(skill);

      skill = new Skill();
      skill.label = "Angular JS";
      skill.full = 4;
      this.frontendSkills.push(skill);

      skill = new Skill();
      skill.label = "HTML";
      skill.full = 5;
      this.frontendSkills.push(skill);

      skill = new Skill();
      skill.label = "CSS/SCSS";
      skill.full = 5;
      this.frontendSkills.push(skill);

      skill = new Skill();
      skill.label = "Bootstrap";
      skill.full = 5;
      this.frontendSkills.push(skill);

      skill = new Skill();
      skill.label = "Websocket/SignalR";
      skill.full = 4;
      this.frontendSkills.push(skill);
      
      //security
      skill = new Skill();
      skill.label = "RSA Encryption";
      skill.full = 5;
      this.security.push(skill);

      skill = new Skill();
      skill.label = "AES Encryption";
      skill.full = 5;
      this.security.push(skill);

      skill = new Skill();
      skill.label = "3/DES Encryption";
      skill.full = 5;
      this.security.push(skill);

      skill = new Skill();
      skill.label = "Bearer Access Token";
      skill.full = 5;
      this.security.push(skill);

      skill = new Skill();
      skill.label = "Anti-Forgery Token";
      skill.full = 5;
      this.security.push(skill);

      //softwares
      skill = new Skill();
      skill.label = "Visual Studio";
      skill.full = 5;
      this.softwares.push(skill);

      skill = new Skill();
      skill.label = "VS Code";
      skill.full = 5;
      this.softwares.push(skill);

      skill = new Skill();
      skill.label = "Android Studio";
      skill.full = 3;
      this.softwares.push(skill);

      skill = new Skill();
      skill.label = "SQL Management Studio";
      skill.full = 5;
      this.softwares.push(skill);

      skill = new Skill();
      skill.label = "Heidi SQL";
      skill.full = 4;
      this.softwares.push(skill);

      skill = new Skill();
      skill.label = "Mongo Compass/Robo 3T";
      skill.full = 5;
      this.softwares.push(skill);

      skill = new Skill();
      skill.label = "Devops";
      skill.full = 5;
      this.softwares.push(skill);

      skill = new Skill();
      skill.label = "Bitbucket";
      skill.full = 5;
      this.softwares.push(skill);

      skill = new Skill();
      skill.label = "SourceTree";
      skill.full = 5;
      this.softwares.push(skill);

      skill = new Skill();
      skill.label = "Trello";
      skill.full = 5;
      this.softwares.push(skill);

      skill = new Skill();
      skill.label = "Jira";
      skill.full = 5;
      this.softwares.push(skill);

      //job descriptions
      let jobDescription = new JobDescription();
      jobDescription.company = "Beauty One International PTE LTD";
      jobDescription.title = "Senior Software Engineer";
      jobDescription.durationFrom = "05/2017";
      jobDescription.durationTo = "06/2022";
      jobDescription.descriptions = new Array<String>();  
      jobDescription.descriptions.push("Working in a development team as full-stack developer. Lead the team in project development & technical solution.");
      jobDescription.descriptions.push("Familiar with Azure cloud services such as App Service, Blob Storage, Mailer, and Logic Apps.");
      jobDescription.descriptions.push("Build an online ordering system that connected to warehousing, logistic, and cms product website, the entire system automate the ordering flow without human intervention.");
      jobDescription.descriptions.push("Develop a scanning program for facial and hair treatment, integrate with third party scanning device in web and windows exe program.");
      jobDescription.descriptions.push("Develop an in-house CMS system which allows user to update the web site's content through the CMS. It supports multi languages.");
      jobDescription.descriptions.push("Using Angular 10, Vue, .Net Core 2, EF Core, HTML, CSS/SCSS, OAuth Access Token, MSSQL, MongoDB, Sourcetree, Azure Cloud.");
      jobDescription.projects = new Array<String>(); 
      jobDescription.projects.push("Ticketing System");
      jobDescription.projects.push("CMS System");
      jobDescription.projects.push("Company Product Websites");
      jobDescription.projects.push("Online Ordering");
      jobDescription.projects.push("eBooklet");
      jobDescription.projects.push("Scanning Program");
      this.jobDescriptions.push(jobDescription);

      jobDescription = new JobDescription();
      jobDescription.company = "ONEPIP (Singapore) Pte Ltd";
      jobDescription.title = "Software Engineer";
      jobDescription.durationFrom = "09/2014";
      jobDescription.durationTo = "05/2017";
      jobDescription.descriptions = new Array<String>(); 
      jobDescription.descriptions.push("Working in the software team, involved in the system development and database design");
      jobDescription.descriptions.push("Familiar with Amazon web serivces such as EC2, S3 and SES.");
      jobDescription.descriptions.push("Develop an enterprise system to support operation team to handle the transaction from customers, the requests are including money transfer and foreign exchange transaction.");
      jobDescription.descriptions.push("Using ASP.Net MVC, Entity Framework, Web API, HTML5, CSS/SCSS, Angular Js, JQuery, MSSQL, MYSQL, AWS, SignalR, Trello");
      jobDescription.projects = new Array<String>(); 
      jobDescription.projects.push("Company Website");
      jobDescription.projects.push("Enterprise System");
      jobDescription.projects.push("Web remittance portal for Personal");
      jobDescription.projects.push("Web remittance portal for Business");
      this.jobDescriptions.push(jobDescription);

      //interships
      jobDescription = new JobDescription();
      jobDescription.company = "Apparel Alliance (Malaysia) Sdn Bhd";
      jobDescription.title = "Embedded Analyst";
      jobDescription.durationFrom = "04/2014";
      jobDescription.durationTo = "07/2014";
      jobDescription.descriptions = new Array<String>(); 
      jobDescription.descriptions.push("Using C/C++ and Arduino board");
      jobDescription.descriptions.push("Develop a Automated Guided Vehicle with observer sensor for safety feature and color detection sensor for routing.");
      jobDescription.descriptions.push("The vehicle is able to skip or stop at the loading point. Once the vehicle stop at the loading point user may need to press a continue button to let the vehicle continue its route.");
      jobDescription.descriptions.push("Troubleshoot hardware and firmware to ensure both are working well together.");
      jobDescription.projects = new Array<String>(); 
      jobDescription.projects.push("Automated Guided Vehicle");
      this.interships.push(jobDescription);

      //educations
      let education = new Education();
      education.school = "Tunku Abdul Rahman College, KL Malaysia";
      education.level = "Bachelor degree of Science";
      education.title = "Microelectronics with Computer Communications";
      education.CGPA = "2.9330";
      education.durationFrom = "09/2012";
      education.durationTo = "03/2014";
      this.educations.push(education);

      education = new Education();
      education.school = "Tunku Abdul Rahman College, KL Malaysia";
      education.level = "Advanced Diploma in Science";
      education.title = "Microelectronics with Computer Communications";
      education.CGPA = "3.0387";
      education.durationFrom = "09/2012";
      education.durationTo = "03/2014";
      this.educations.push(education);

      education = new Education();
      education.school = "Tunku Abdul Rahman College, KL Malaysia";
      education.level = "Diploma in Science";
      education.title = "Microelectronics with Computer Communications";
      education.CGPA = "3.0493";
      education.durationFrom = "09/2010";
      education.durationTo = "09/2012";
      this.educations.push(education);

      education = new Education();
      education.school = "Tunku Abdul Rahman College, KL Malaysia";
      education.level = "Certificate in Electronic and Computer Technology";
      education.title = "Microelectronic";
      education.CGPA = "3.375";
      education.durationFrom = "05/2009";
      education.durationTo = "09/2010";
      this.educations.push(education);
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }
}

class Skill {
  label: String;
  full: Number;
  half: Number = 0;
}

class JobDescription {
  title: String;
  position: String;
  durationFrom: String;
  durationTo: String;
  company: String;
  descriptions: Array<String>;
  projects: Array<String>;
}

class Education {
  title: String;
  durationFrom: String;
  durationTo: String;
  school: String;
  level: String;
  CGPA: String;
}

