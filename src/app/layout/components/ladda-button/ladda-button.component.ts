import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'ladda-button',
    templateUrl: './ladda-button.component.html',
    styleUrls: ['./ladda-button.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class LaddaButtonComponent {
    @Input() loading: Boolean = false;
    @Input() color: string = 'primary';
    @Input() type: string = 'button';
    @Input() label: string;
    @Input() icon: string;

    @Output() onClick = new EventEmitter<any>();

    handleClick(event) {
        this.onClick.emit(event);
    }
}