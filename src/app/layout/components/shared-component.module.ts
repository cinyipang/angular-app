import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { LaddaModule } from 'angular2-ladda';
import { LaddaButtonComponent } from './ladda-button/ladda-button.component';
import { RoundedImageComponent } from './rounded-image/rounded-image.component';
import { CustomeFileComponent } from './custome-file/custom-file.component';

@NgModule({
    imports: [ 
        CommonModule, 
        FormsModule,
        LaddaModule.forRoot({
            spinnerSize: 30,
            spinnerColor: "white",
            spinnerLines: 10
        }),
    ],
    declarations: [
        CustomeFileComponent,
        RoundedImageComponent,
        LaddaButtonComponent
    ],
    exports: [
        CustomeFileComponent,
        RoundedImageComponent,
        LaddaButtonComponent
    ]
})
  
export class ShredComponentModule {}