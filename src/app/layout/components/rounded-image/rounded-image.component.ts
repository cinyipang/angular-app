import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-rounded-image',
  templateUrl: './rounded-image.component.html',
  styleUrls: ['./rounded-image.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RoundedImageComponent {

  constructor() { }

  @Input() src: string;
  @Input() styles: any = { width:'50px', height:'50px' };

  @Output() onClick = new EventEmitter<any>();

  handleClick(event) {
      this.onClick.emit(event);
  }

}
