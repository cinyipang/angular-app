import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-custom-file',
  templateUrl: './custom-file.component.html',
  styleUrls: ['./custom-file.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CustomeFileComponent {

  constructor() { }

  @Input() loading: Boolean;
  @Input() id: String;
  @Input() label: String;
  @Output() onChange = new EventEmitter<any>();
  @Output() onUpload = new EventEmitter<any>();

  handleChange(event) {
      this.onChange.emit(event);
      if(event.target.files.length)
      {
          var fileName = event.target.files[0].name;
          var nextSibling = event.target.nextElementSibling
          nextSibling.innerText = fileName
      }
  }

  handleUpload(event) {
      this.onUpload.emit(event);
  }
}
