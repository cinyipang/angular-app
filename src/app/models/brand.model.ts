import { Pagination } from './pagination.model';

class Brand {
    brandId: String;
    code: String;
    name: String;
    description: String;
    //audit
    status: String;
    createdBy: String;
    createdOn: Date;
    modifiedBy: String;
    modifiedOn: Date;
}


class Brands {
    items: Array<Brand> = new Array<Brand>();
    pagination: Pagination = new Pagination();
}

export {
    Brand,
    Brands
}