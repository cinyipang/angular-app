import { Pagination } from './pagination.model';
import { RolePermission } from './rolePermission.model';

class Role {
    roleId: String;
    name: String;
    rolePermissions: RolePermission[];
    //audit
    createdBy: String;
    createdOn: Date;
    modifiedBy: String;
    modifiedOn: Date;
}


class Roles {
    items: Array<Role> = new Array<Role>();
    pagination: Pagination = new Pagination();
}

export {
    Role,
    Roles
}