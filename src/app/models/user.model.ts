import { Pagination } from './pagination.model';
import { Role } from './role.model';

class User {
    userId: String;
    username: String;
    name: String;
    email: String;
    status: String;
    remarks: String;
    profilePicture: String; 
    role: Role;
    //audit
    createdBy: string;
    createdOn: Date;
    modifiedBy: string;
    modifiedOn: Date;
}

class Users {
    items: Array<User> = new Array<User>();
    pagination: Pagination = new Pagination();
}

export {
    User,
    Users
}