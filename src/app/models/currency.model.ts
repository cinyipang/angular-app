import { Pagination } from './pagination.model';

class Currency {
    currencyId: String;
    code: String;
    name: String;
    //audit
    status: String;
    createdBy: String;
    createdOn: Date;
    modifiedBy: String;
    modifiedOn: Date;
}

class Currencies {
    items: Array<Currency> = new Array<Currency>();
    pagination: Pagination = new Pagination();
}

export {
    Currency,
    Currencies
}