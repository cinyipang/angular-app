
export class Pagination {
    page: Number = 1;
    pageSize: Number = 3;
    asc: Boolean = false;
    order: String = 'CreatedOn';
    totalEntries: Number;
    nextPage: Number;
    previousPage: Number;
    totalPages: Number;
    operation: String = 'And' || 'Or';
    params: Param[];
}

export class Param {
    type: String = 'Contains' || 'Equals' || 'NotEquals' || 'DateFrom' || 'DateTo';
    key: String;
    value: String;
}
