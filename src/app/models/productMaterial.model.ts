
export class ProductMaterial {
    productMaterialId: String;
    materialId: String;
    productId: String;
    quantity: String;
    //audit
    status: String;
    createdBy: String;
    createdOn: Date;
    modifiedBy: String;
    modifiedOn: Date;
}
