import { Currency } from './currency.model';
import { Pagination } from './pagination.model';

class Material {
    materialId: String;
    name: String;
    code: String;  
    units: String;  
    cost: String;
    remarks: String;
    description: String;
    currency: Currency;
    //audit
    status: String;
    createdBy: String;
    createdOn: Date;
    modifiedBy: String;
    modifiedOn: Date;
}

class Materials {
    items: Array<Material> = new Array<Material>();
    pagination: Pagination = new Pagination();
}

export {
    Material,
    Materials
}