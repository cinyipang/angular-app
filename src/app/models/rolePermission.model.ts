import { Pagination } from './pagination.model';

class RolePermission {
    rolePermissionId: String;
    module: String;
    access: string[];
    //audit
    createdBy: String;
    createdOn: Date;
    modifiedBy: String;
    modifiedOn: Date;
}


class RolePermissions {
    items: Array<RolePermission> = new Array<RolePermission>();
    pagination: Pagination = new Pagination();
}

export {
    RolePermission,
    RolePermissions
}