import { Pagination } from './pagination.model';
import { Currency } from './currency.model';
import { Brand } from './brand.model';
import { ProductMaterial } from './productMaterial.model';

class Product {
    productId: String;
    name: String;
    code: String;
    cost: String;
    price: String;
    minOfQuantity: Number;
    isFeatured: Boolean;
    remarks: String;
    standardCarton: Number;
    productMaterials: ProductMaterial[];
    currency: Currency;
    brand: Brand;    
    //audit
    status: string;
    createdBy: string;
    createdOn: Date;
    modifiedBy: string;
    modifiedOn: Date;
}

class Products {
    items: Array<Product> = new Array<Product>();
    pagination: Pagination = new Pagination();
}

export {
    Product,
    Products
}