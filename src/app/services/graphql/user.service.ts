import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserGraphqlService {  
  constructor(private http: HttpClient) {   
  }
  
  get(): Observable<any>{
    return this.http.post<any>(`${environment.grapql.endpoint}`,
    JSON.stringify({
      query: `
      query {
        user(code:"IT") {
          _id
          name
          departmentCode
          status
        }
      }`
    }),
    { 
      headers: { 'Content-Type': 'application/json' }
    });
  }
  
  findAll(): Observable<any>{
    return this.http.post<any>(`${environment.grapql.endpoint}`,
    JSON.stringify({
      query: `
      query {
        users {
          _id
          name
          departmentCode
          status
        }
      }`
    }),
    { 
      headers: { 'Content-Type': 'application/json' }
    });
  }
}