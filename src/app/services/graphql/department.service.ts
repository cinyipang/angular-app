import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DepartmentGraphqlService {  
  constructor(private http: HttpClient) {   
  }
  
  get(): Observable<any>{
    return this.http.post<any>(`${environment.grapql.endpoint}`,
    JSON.stringify({
      query: `
      query {
        department(code: "IT") {
          _id
          code
          title
        }
      }`
    }),
    { 
      headers: { 'Content-Type': 'application/json' }
    });
  }
  
  findAll(): Observable<any>{
    return this.http.post<any>(`${environment.grapql.endpoint}`,
    JSON.stringify({
      query: `
      query {
        departments {
          _id
          code
          title
        }
      }`
    }),
    { 
      headers: { 'Content-Type': 'application/json' }
    });
  }
  
  add(): Observable<any>{
    return this.http.post<any>(`${environment.grapql.endpoint}`,
    JSON.stringify({
      query: `
        mutation {
          addDepartment(title: "Human Resource", code: "HR") {
            _id
            code
            title
          }
        }`
    }),
    { 
      headers: { 'Content-Type': 'application/json' }
    });
  }
}