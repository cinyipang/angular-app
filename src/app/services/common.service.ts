import { Injectable } from '@angular/core';
import { NgbDateStruct, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
    providedIn: 'root'
})
export class CommonService {
    constructor() { }

    public isToggleMenu: boolean = false;

    //yyyy-MM-dd
    public parseDateStruct(value: string): NgbDateStruct {
        if (!value) { return null; }
    
        const parts = value.trim().split('-');
        
        return {
            year: parts.length > 0 ? parseInt(parts[0], 10) : null,
            month: parts.length > 1 ? parseInt(parts[1], 10) : null,
            day: parts.length > 2 ? parseInt(parts[2], 10) : null,
        };
    }

    public parseDateTime(ngdate: NgbDateStruct, ngTime: NgbTimeStruct): Date {
        return new Date(`${ngdate.year}-${ngdate.month}-${ngdate.day} ${ngTime.hour}:${ngTime.minute}:${ngTime.second}`);
    }

    public getFileName(contentDisposition: string): string {
        var fileName = '';
        if (contentDisposition && contentDisposition.indexOf('attachment') !== -1) {
            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            var matches = filenameRegex.exec(contentDisposition);
            if (matches != null && matches[1]) { 
                fileName = matches[1].replace(/['"]/g, '');
            }
        }
        return fileName;
    }

    public dataUriToBlob(dataUri) {
        let byteString;
        if (dataUri.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataUri.split(',')[1]);
        else
            byteString = unescape(dataUri.split(',')[1]);
            
        console.log(byteString.length);

        // separate out the mime component
        const mimeString = dataUri.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        const ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type:mimeString});
    }

    public readFileToBase64(file) {
        return new Promise((resolve, reject) => {
          let reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => {
            resolve(reader.result);
          };
          reader.onerror = reject;
        });
    }

    public back(): void {
        window.history.back();
    }
}