import { AuthService } from './oauth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { UserApiService } from './api';
import { User } from '../models';
import { retry } from 'rxjs/operators';
 
@Injectable()
export class GuardService implements CanActivate { 
    
    currentUser = new User();

    constructor(
        private authService: AuthService,
        private userApi: UserApiService,
        private router: Router
    ) { 
        this.userApi.getCurrentUserObservable().subscribe(
            res => {
                this.currentUser = res;
            }
        );
    }

    async canActivate(route: ActivatedRouteSnapshot) {
        if (!this.authService.isAuthenticated()) 
            window.location.href = '/login';
        
        const routePermission = route.data.permission;
        if(routePermission === undefined) 
            return true;

        if(this.currentUser === null)
            this.currentUser = await this.userApi.currentAsync();
        
        console.log(routePermission, this.currentUser.role.rolePermissions);

        //check module in user role
        const rolePermission = this.currentUser.role.rolePermissions.find(x => x.module === routePermission.module);
        if (rolePermission === undefined) {
            this.router.navigate(['/home']);
            return false;
        }
        //check access
        const userAccess = rolePermission.access.find(x => x === routePermission.access);
        if (userAccess === undefined) {
            this.router.navigate(['/home']);
            return false;
        }
        
        return true;
    }
}