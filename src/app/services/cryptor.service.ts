import { Injectable } from '@angular/core';

import * as cryptor from 'crypto-js';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CryptorService {
    constructor() { }

    //aes encode - for posting
    public AesEncrypt(value : string): string {
        var aesKey = cryptor.enc.Utf8.parse(environment.aes.key);  
        var aesIv = cryptor.enc.Utf8.parse(environment.aes.iv);  
        var options = {
            iv: aesIv,
            mode: cryptor.mode.CBC,
            padding: cryptor.pad.Pkcs7
        };

        var encrypted = cryptor.AES.encrypt(value, aesKey, options);
        var encryptedBase64 = encrypted.toString();
        return encryptedBase64;
    }
    //aes decode
    public AesDecrypt(value : string): string {
        var aesKey = cryptor.enc.Utf8.parse(environment.aes.key);  
        var aesIv = cryptor.enc.Utf8.parse(environment.aes.iv);  
        var options = {
            iv: aesIv,
            mode: cryptor.mode.CBC,
            padding: cryptor.pad.Pkcs7
        };

        var decrypted = cryptor.AES.decrypt(value, aesKey, options);
        return decrypted.toString(cryptor.enc.Utf8);
    }

    //des encode - for url
    public DesEncrypt(value: string): string {
        var deskey = cryptor.enc.Utf8.parse(environment.des.key);  
        var desiv = cryptor.enc.Utf8.parse(environment.des.iv);
        var options = {
            iv: desiv,
            mode: cryptor.mode.CBC,
            padding: cryptor.pad.Pkcs7
        };

        var encrypted = cryptor.DES.encrypt(value, deskey, options);
        var encryptedBase64 = encrypted.toString();
        return this.Base64UrlTokenEncode(encryptedBase64);
    }
    //des decode
    public DesDecrypt(value: string): string {
        var deskey = cryptor.enc.Utf8.parse(environment.des.key);  
        var desiv = cryptor.enc.Utf8.parse(environment.des.iv);
        var options = {
            iv: desiv,
            mode: cryptor.mode.CBC,
            padding: cryptor.pad.Pkcs7
        };

        var encryptedBase64 = this.Base64UrlTokenDecode(value)
        var decrypted = cryptor.DES.decrypt(encryptedBase64, deskey, options);
        return decrypted.toString(cryptor.enc.Utf8);
    }

    //sha512 - for password
    public Sha512(value: string): string{
        var hashValue = cryptor.SHA512(value);
        value = hashValue.toString(cryptor.enc.Base64);
        return value.toString();
    }

    public Base64UrlTokenEncode(token): string {
        var ret = token.replace(/\+/g, "-").replace(/\//g, "_");
    
        var noPadding = ret.replace(/=+$/, ""); 
    
        return noPadding + (ret.length - noPadding.length).toString();
    }
    
    public Base64UrlTokenDecode(token): string {
        if (token.length == 0) 
            return null;

        //The last character in the token is the number of padding characters.
        var numberOfPadding = token.slice(-1);

        //The Base64 string is the token without the last character.
        token = token.slice(0, -1);

        //'-'s are '+'s and '_'s are '/'s.
        token = token.replace(/-/g, '+');
        token = token.replace(/_/g, '/');

        //Pad the Base64 string out with '='s
        for (var i = 0; i < numberOfPadding; i++)
            token += "=";

        return token;
    }
}