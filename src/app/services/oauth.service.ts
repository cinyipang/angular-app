import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';
import { CryptorService } from './cryptor.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public authUser = {
    permissions: [],
    name: "",
    userId: "",
  };

  public storageKeys: any = {
    accessToken: 'access_token',
    expiresAt: 'expires_at'
  }

  headers = new HttpHeaders({
    'Content-Type':  'application/json',
  });

  constructor(public router: Router, private http: HttpClient, private cryptor: CryptorService) {   
      var token = localStorage.getItem(this.storageKeys.accessToken);
      if (token) {
        var jwtHelper = new JwtHelperService();
        var decodedToken = jwtHelper.decodeToken(token);
        this.authUser.name = decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'];
        this.authUser.userId = decodedToken.Id;
      }
  }

  private ngUnsubscribe: Subject<any> = new Subject();
    ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  public login(username: string, password: string) {
    //sha
    password = this.cryptor.Sha512(password);    
    //aes encrypt
    password = this.cryptor.AesEncrypt(password);

    const loginBody = {      
      Username: username,
      Password: password,
      ClientId: environment.auth.clientId
    };
    
    return this.http.post(environment.auth.issuer + "/api/identities/token",loginBody, { headers: this.headers });
  }
  
  // Remove tokens and expiry time from localStorage
  public logout(): void {
    this.clearAuthentication();
    window.location.href  = '/login';
  }

  public clearAuthentication(): void{
    localStorage.removeItem(this.storageKeys.accessToken);
    localStorage.removeItem(this.storageKeys.expiresAt);
  }

  // Check whether the current time is past the    // access token's expiry time
  public isAuthenticated(): boolean {
    const expiresAt = JSON.parse(localStorage.getItem(this.storageKeys.expiresAt));
    const totalSeconds = new Date().getTime() / 1000;
    return localStorage.getItem(this.storageKeys.accessToken) && (expiresAt > totalSeconds);
  }

  public getAccessToken(): string {
    return localStorage.getItem(this.storageKeys.accessToken);
  }

  public hasAccess(modudle: string, action: string): boolean {
    if (!modudle) return false;
    if (!action) return false;
    if (!this.authUser.permissions) return false;

    for (let p of this.authUser.permissions) {
      for(let a of p.actions) {
        if (p.module && p.module.toLowerCase() == modudle.toLowerCase() && a && a.toLowerCase() == action.toLowerCase()) 
          return true;
      }
    }
    return false;
  }
}