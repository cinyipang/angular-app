import { Injectable } from '@angular/core';
import { UserApiService } from './api';
import { User } from 'src/app/models';

@Injectable()
export class PermissionService {  
    constructor(private userApi: UserApiService) {  
        this.userApi.getCurrentUserObservable().subscribe(
            res => {
                this.currentUser = res;
            }
        );
    }

    currentUser: User = new User();
    
    async hasAccess(module, access){
        if(this.currentUser === null)
            this.currentUser = await this.userApi.currentAsync();
            
        const rolePermission = this.currentUser.role.rolePermissions.find(x => x.module === module);
        if(rolePermission === undefined)
            return false;
        
        const hasAccess = rolePermission.access.find(x => x === access);
        if(hasAccess === undefined)
            return false;
            
        return true;
    }
}