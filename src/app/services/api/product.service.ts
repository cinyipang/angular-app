
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Product, Products } from 'src/app/models';

@Injectable({
    providedIn: 'root'
})
export class ProductApiService {  
    constructor(private http: HttpClient) {   
    }

    find(body: any): Observable<Products>{
        return this.http.post<Products>(`${environment.auth.issuer}/api/products/find`, body);
    }

    get(id: String): Observable<Product>{
        return this.http.get<Product>(`${environment.auth.issuer}/api/products/${id}`);
    }

    add(body: any): Observable<Product>{
        return this.http.post<Product>(`${environment.auth.issuer}/api/products`, body);
    }

    update(id: String, body: any): Observable<Product>{
        return this.http.put<Product>(`${environment.auth.issuer}/api/products/${id}`, body);
    }

    delete(id: String): Observable<Product>{
        return this.http.delete<Product>(`${environment.auth.issuer}/api/products/${id}`);
    }
}