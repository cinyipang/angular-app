import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Currency, Currencies } from 'src/app/models';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CurrencyApiService {  
    constructor(private http: HttpClient) {   
    }

    find(body: any): Observable<Currencies>{
        return this.http.post<Currencies>(`${environment.auth.issuer}/api/currencies/find`, body);
    }

    get(id: String): Observable<Currency>{
        return this.http.get<Currency>(`${environment.auth.issuer}/api/currencies/${id}`);
    }

    add(body: any): Observable<Currency>{
        return this.http.post<Currency>(`${environment.auth.issuer}/api/currencies`, body);
    }

    update(id: String, body: any): Observable<Currency>{
        return this.http.put<Currency>(`${environment.auth.issuer}/api/currencies/${id}`, body);
    }

    delete(id: String): Observable<Currency>{
        return this.http.delete<Currency>(`${environment.auth.issuer}/api/brands/${id}`);
    }
}