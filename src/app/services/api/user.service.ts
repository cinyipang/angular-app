
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from 'src/app/models';

@Injectable({
    providedIn: 'root'
})
export class UserApiService {  
    
    private currentUserSource: BehaviorSubject<User> = new BehaviorSubject<User>(null);

    constructor(private http: HttpClient) {   
        this.current()
        .subscribe(res => {
            console.log("current", res);
            this.currentUserSource.next(res);
        });
    }

    getCurrentUserObservable(): Observable<User> {
      return this.currentUserSource.asObservable();
    }

    updateCurrentUserObservable(newValue: User) {
        this.currentUserSource.next(newValue);
    }

    //api
    current(): Observable<User>{
        return this.http.get<User>(`${environment.auth.issuer}/api/users/current`);
    }

    currentAsync(): Promise<User>{
        return new Promise((resolve, reject) => {
            return this.http.get<User>(`${environment.auth.issuer}/api/users/current`)
            .subscribe(
                res => {
                    resolve(res);
                },
                err => {
                    reject(err);
                }
            )
        });
    }

    profile(body: any): Observable<User>{
        return this.http.put<User>(`${environment.auth.issuer}/api/users/profile`, body);
    }

    profilePicture(formData: FormData){
        return this.http.post<any>(`${environment.auth.issuer}/api/users/profile_picture`, formData);
    }
}