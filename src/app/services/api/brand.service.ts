import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Brand, Brands } from 'src/app/models';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BrandApiService {  
    constructor(private http: HttpClient) {   
    }
    
    find(body: any): Observable<Brands>{
        return this.http.post<Brands>(`${environment.auth.issuer}/api/brands/find`, body);
    }

    get(id: String): Observable<Brand>{
        return this.http.get<Brand>(`${environment.auth.issuer}/api/brands/${id}`);
    }

    add(body: any): Observable<Brand>{
        return this.http.post<Brand>(`${environment.auth.issuer}/api/brands`, body);
    }

    update(id: String, body: any): Observable<Brand>{
        return this.http.put<Brand>(`${environment.auth.issuer}/api/brands/${id}`, body);
    }

    delete(id: String): Observable<Brand>{
        return this.http.delete<Brand>(`${environment.auth.issuer}/api/brands/${id}`);
    }
}