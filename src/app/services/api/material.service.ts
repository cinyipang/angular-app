import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Material, Materials } from 'src/app/models';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MaterialApiService {  
    constructor(private http: HttpClient) {   
    }

    find(body: any): Observable<Materials>{
        return this.http.post<Materials>(`${environment.auth.issuer}/api/materials/find`, body);
    }

    get(id: String): Observable<Material>{
        return this.http.get<Material>(`${environment.auth.issuer}/api/materials/${id}`);
    }

    add(body: any): Observable<Material>{
        return this.http.post<Material>(`${environment.auth.issuer}/api/materials`, body);
    }

    update(id: String, body: any): Observable<Material>{
        return this.http.put<Material>(`${environment.auth.issuer}/api/materials/${id}`, body);
    }

    delete(id: String): Observable<Material>{
        return this.http.delete<Material>(`${environment.auth.issuer}/api/brands/${id}`);
    }
}