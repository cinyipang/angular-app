import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import FileSaver from 'file-saver';
import { CommonService } from 'src/app/services/common.service';

@Injectable({
    providedIn: 'root'
})
export class MediaApiService {  
    constructor(private http: HttpClient, private common: CommonService) {   
    }

    uploadCsv(formData: FormData){
        return this.http.post(`${environment.auth.issuer}/api/media/upload/csv`, formData);
    }

    downloadCsv(){
        return new Promise((resolve, reject) => {
            let params = new HttpParams().set('type', 'testing')
            this.http.get(`${environment.auth.issuer}/api/media/download/csv`, 
            { 
                params: params, 
                responseType: 'arraybuffer', 
                observe: 'response' 
            })
            .subscribe(
                res => {
                    console.log(res);
                    const headers = res.headers;
                    const contentType = headers.get('content-type');
                    const contentDisposition = headers.get('content-disposition');
                    const filename = this.common.getFileName(contentDisposition);
                    const blob = new Blob([res.body], { type: contentType });
                    FileSaver.saveAs(blob, filename); 
                    resolve(true);
                }
            )
        });
    }

    uploadExcel(formData: FormData){
        return this.http.post(`${environment.auth.issuer}/api/media/upload/excel`, formData);
    }

    downloadExcel(params: HttpParams){
        return new Promise((resolve, reject) => {
            this.http.get(`${environment.auth.issuer}/api/media/download/excel`, 
            { 
                params: params, 
                responseType: 'arraybuffer', 
                observe: 'response' 
            })
            .subscribe(
                res => {
                    console.log(res);   
                    const headers = res.headers;
                    const contentType = headers.get('content-type');
                    const contentDisposition = headers.get('content-disposition');
                    const filename = this.common.getFileName(contentDisposition);
                    const blob = new Blob([res.body], { type: contentType });
                    FileSaver.saveAs(blob, filename); 
                    resolve(true);
                },
                error => {
                    reject();
                    console.log("error", error);
                }
            );

        });
    }

    downloadPdf(params: HttpParams){
        return new Promise((resolve, reject) => {
            this.http.get(`${environment.auth.issuer}/api/media/download/pdf`, 
            { 
                params: params, 
                responseType: 'arraybuffer', 
                observe: 'response' 
            })
            .subscribe(
                res => { 
                    console.log(res);  
                    const headers = res.headers;
                    const contentType = headers.get('content-type');
                    const contentDisposition = headers.get('content-disposition');
                    const filename = this.common.getFileName(contentDisposition);    
                    const blob = new Blob([res.body], { type: contentType });
                    FileSaver.saveAs(blob, filename);         
                    resolve(true);
                },
                error => { 
                    reject();
                    console.log("error", error);
                }
            );
        }); 
    }
}